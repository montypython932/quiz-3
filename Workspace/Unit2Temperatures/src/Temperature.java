import java.util.Scanner;
public class Temperature {
	
	static String[] months = new String[] {"Jan", "Feb", "March", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
	//Integer array of low and high temps
	static int[][] temps = new int[12][2];

	public static void main(String[] args) {
		inputTempForYear();
		System.out.println("Average High: " + calculateAverageHigh(temps));
		System.out.println("Average Low: " + calculateAverageLow(temps));
		System.out.println("Lowest Temp: " + temps[findLowestTemp(temps)][0] + " in " + months[findLowestTemp(temps)]);
		System.out.println("Highest Temp: " + temps[findHighestTemp(temps)][1] + " in " + months[findHighestTemp(temps)]);
	}
	
	//inputs the temps for a given month, called by inputTempForYear()
	public static void inputTempForMonth(int month, int[][] temps) {
		System.out.println("Please enter the low for " + months[month]);
		Scanner input = new Scanner(System.in);
		temps[month][0] = input.nextInt();
		System.out.println("Please enter the high for " + months[month]);
		temps[month][1] = input.nextInt();
	}
	
	//loops through temps array, calling inputTempForMonth() to get high and low for a given month
	public static int[][] inputTempForYear() {
		for (int i = 0; i < 12; i++) {
			inputTempForMonth(i, temps);
		}
		return temps;
	}
	
	//as the name suggests, it calculates the average high
	public static int calculateAverageHigh(int[][] temps) {
		int average = 0;
		for(int i = 0; i < temps.length; i++) {
			average += temps[i][1]; 
		}
		average /= temps.length;
		return average;
	}
	
	//as the name suggests, it calculates the average low
	public static int calculateAverageLow(int[][] temps) {
		int average = 0;
		for(int i = 0; i < temps.length; i++) {
			average += temps[i][0]; 
		}
		average /= temps.length;
		return average;
	}
	
	//find the lowest temp by searching through [i][0] indexes of array temps
	public static int findLowestTemp(int[][] temps) {
		int lowest = temps[0][0];
		int lowestIndex = 0;
		for (int i = 0; i < temps.length - 1; i++) {
			if (temps[i][0] < lowest) {
				lowest = temps[i][0];
				lowestIndex = i;
			}
		}
		return lowestIndex;
	}
	
	//find the lowest temp by searching through [i][1] indexes of array temps
	public static int findHighestTemp(int[][] temps) {
		int highest = temps[0][0];
		int highestIndex = 0;
		for (int i = 0; i < temps.length - 1; i++) {
			if (temps[i][0] > highest) {
				highest = temps[i][1];
				highestIndex = i;
			}
		}
		return highestIndex;
	}
}
