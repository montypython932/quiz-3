import java.util.Scanner;

public class sorter {
	
	static int[] input = new int[50];
	static int[] sorted = new int[50];
	
	public static void main(String[] args) {
		getInput();
		sort(input);
		ascending(input);
		descending(input);
	}
	
	//gets input from the user, ints only
	public static void getInput() {
		Scanner console = new Scanner(System.in);
		for (int i = 0; i < 50; i++) {
			input[i] = console.nextInt();
		}
	}
	
	//sorts array into ascending order
	public static void sort(int[] nums) {
		int least;
		for (int j = 0; j < nums.length; j++) {
			for (int i = 1; i < nums.length; i++) {
				if (nums[i] < nums[i - 1]) {
					least = nums[i];
					nums[i] = nums[i - 1];
					nums[i - 1] = least;
				}
			}
		}
	}
	
	//prints out numbers from an array in ascending order
	public static void ascending(int[] nums) {
		System.out.println("Nums in ascending order: \n");
		for (int i = 0; i < nums.length; i++) {
			System.out.println(nums[i]);
		}
	}
	
	//prints out numbers from an array in descending order
	public static void descending(int[] nums) {
		System.out.println("Nums in descending order: \n");
		for (int i = nums.length - 1; i > 0; i--) {
			System.out.println(nums[i]);
		}
	}
}
